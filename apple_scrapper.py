import pandas as pd
import numpy as np
import json

from app_store_scraper import AppStore
apple_uber = AppStore(country='fr', app_name='uber-eats-livraison-de-repas', app_id = '1058959277')
apple_macdo = AppStore(country='fr', app_name='mcdonalds', app_id = '1217507712')
for applerev in [apple_uber,apple_macdo]:
    applerev.review(how_many=10000)
    df = pd.DataFrame(applerev.reviews)
    df['year'] = df['date'].dt.year
    new_df = df[["year", "rating", "review", "title"]]
    new_df.to_csv("CSV/" + applerev.app_name + "_apple.csv", index=False)