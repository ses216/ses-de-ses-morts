
import pandas as pd
import numpy as np

from google_play_scraper import reviews_all,reviews

#result = reviews_all('com.kayak.android')
for app in ['com.ubercab.eats','com.md.mcdonalds.gomcdo']:
    result, continuation_token = reviews(app,count=10000,country="fr",lang="fr")
    df = pd.DataFrame(result)
    df['year'] = df['at'].dt.year
    new_df = df[["year", "score", "content"]]
    new_df.columns = ["year", "rating", "review"]

    new_df.to_csv("CSV/" + app.split('.')[1] + "_android.csv", index=False)